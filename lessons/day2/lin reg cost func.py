import numpy as np
import matplotlib.pyplot as plt

x = np.arange(-1, 1, .05)
y = np.random.normal(0, 0.1, x.size) + x*0.5 - 1
# y[5] = 50

slopes = np.arange(-2.0, 2.0+0.1, 0.2)
print(slopes)

yy = x*slopes.reshape((-1, 1))
print(yy[1][0:2])
print(y[0:2])

diff = yy - y
sq = diff*diff
print(diff[1][0:2])
print(sq[1][0:2])

sum = np.sum(sq, axis=1)
print(sum)

plt.scatter(x, y)
for i in range(0, slopes.size):
    plt.plot(x, yy[i])

plt.show()

plt.plot(slopes, sum)
plt.axvline(x=0,dashes=(5,5))
plt.axhline(y=0)
deriva = np.sum(diff*x, axis=1)
plt.plot(slopes, deriva)

plt.show()

yy = 0*x+slopes.reshape((-1, 1))
diff = yy - y
sq = diff*diff
sum = np.sum(sq, axis=1)
plt.scatter(x, y)
for i in range(0, slopes.size):
    plt.plot(x, yy[i])
plt.show()

plt.plot(slopes, sum)
plt.axvline(x=0)
plt.axhline(y=0)
derivb = np.sum(diff, axis=1)
plt.plot(slopes, derivb)
plt.show()

intercept = np.ones((x.size,1))
xi = np.concatenate((intercept, x.reshape(-1, 1)), axis=1)
corr = np.dot(xi.T,xi)
a = np.linalg.inv(corr)
c = np.dot(xi.T,y)
b = np.dot(c,a)
print(b)

yy = b[1]*x + b[0]
plt.scatter(x, y)
plt.plot(x, yy)
plt.show()