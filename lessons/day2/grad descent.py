import numpy as np
import random
import sklearn
from sklearn.datasets.samples_generator import make_regression 
from matplotlib import pyplot as plt
from scipy import stats
import time

def gradient_descent(alpha, x, y, ep=0.0001, max_iter=10000):
    converged = False
    iter = 0
    m = x.shape[0] # number of samples

    # initial theta
    t0 = np.random.random(x.shape[1])
    t1 = np.random.random(x.shape[1])

    # total error, J(theta)
    J = sum([(t0 + t1*x[i] - y[i])**2 for i in range(m)])

    # Iterate Loop
    while not converged:
        # for each training sample, compute the gradient (d/d_theta j(theta))
        grad0 = 1.0/m * sum([(t0 + t1*x[i] - y[i]) for i in range(m)]) 
        grad1 = 1.0/m * sum([(t0 + t1*x[i] - y[i])*x[i] for i in range(m)])

        # update the theta_temp
        temp0 = t0 - alpha * grad0
        temp1 = t1 - alpha * grad1
    
        # update theta
        t0 = temp0
        t1 = temp1

        if(iter in [1, 2, 3, 10]):
            plot(x, t0, t1)

        # mean squared error
        e = sum( [ (t0 + t1*x[i] - y[i])**2 for i in range(m)] ) 

        if abs(J-e) <= ep:
            print ('Converged, iterations: ', iter, '!!!')
            converged = True
    
        J = e   # update error 
        iter += 1  # update iter
    
        if iter == max_iter:
            print ('Max interactions exceeded!')
            converged = True

    return t0,t1

def plot(x, theta0, theta1):
    for i in range(x.shape[0]):
        y_predict = theta0 + theta1*x 

    plt.plot(x,y,'o')
    plt.plot(x,y_predict,'k-')
    # plt.show(block=False)
    plt.draw()
    plt.pause(0.5)

if __name__ == '__main__':

    x, y = make_regression(n_samples=100, n_features=1, n_informative=1, 
                        random_state=0, noise=35) 
    print ('x.shape = %s y.shape = %s' %(x.shape, y.shape))
 
    alpha = 0.1 # learning rate
    ep = 0.01 # convergence criteria

    # call gredient decent, and get intercept(=theta0) and slope(=theta1)
    theta0, theta1 = gradient_descent(alpha, x, y, ep, max_iter=1000)
    print ('theta0 = %s theta1 = %s' %(theta0, theta1))     

    start = time.time()
    n = 100
    for i in range(0, n):
        u = np.ones((x.shape[0], 1))
        xu = np.append(u, x, axis=1)
        xtr = np.transpose(xu)
        xx = np.dot(xtr, xu)
        xy = np.dot(xtr, y)
        inv = np.linalg.inv(xx)
        b = np.dot(inv, xy)
    end = time.time()                
    print ('theta0 = %s theta1 = %s, time = %s/%s' %(b[0], b[1], end-start, n))
       
    # plot
    plot(x, theta0, theta1)
    plt.show()
    print ("Done!")