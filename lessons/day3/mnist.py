import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import svm

def visualize(x, y, i):
    # The first column is the label
    label = y[i][0]

    # The rest of columns are pixels
    pixels = 255 - x[i][1:]

    # Make those columns into a array of 8-bits pixels
    # This array will be of 1D with length 784
    # The pixel intensity values are integers from 0 to 255
    pixels = np.array(pixels, dtype='uint8')

    # Reshape the array into 28 x 28 array (2-dimensional array)
    pixels = pixels.reshape((28, 28))

    # Plot
    plt.title('Label is {label}'.format(label=label))
    plt.imshow(pixels, cmap='gray')
    plt.show()


# def linreg_gd(xi, y, d):
#     b = np.random.random(xi.shape[1])/100000
#     y2 = [1 if y[i] == d else 0 for i in range(y.size)]
#     alpha = xi.shape[1] * y.size / 2
#     i = 0
#     J0 = 0
#     for s in range(1000):
#        yy = np.dot(xi, b)
#        diff = yy - y2
#        J = np.dot(diff.T, diff)
#        if(i%50 == 0):
#            print("step %s, J = %s, delta = %s" %(i, J, J0 - J))
#        trans = xi.T
#        dot = np.dot(trans, diff)
#        grad = dot / y.size
#        b -= grad / alpha
#        i += 1
#        J0 = J
#     return b

class Model(object):

    def Train(self, xi, y):
        raise NotImplementedError("Should have implemented this")

    def Predict(self, xit, dgt):
        raise NotImplementedError("Should have implemented this")

class LinearModel(Model):

    def Train(self, xi, y):
        print("Calculating correlation matrix...")
        corr = np.dot(xi.T, xi)
        print("Done.")

        a = np.linalg.pinv(corr)
        print("Calculated inverse matrix.")

        self.b = np.zeros((10, xi.shape[1]))

        for dgt in range(10):
            y2 = [1 if y[i] == dgt else 0 for i in range(y.size)]
            c = np.dot(xi.T, y2)
            self.b[dgt] = np.dot(a, c)

        print("Trained model for all digits.")
        return self.b

    def Predict(self, xit, dgt):
        yy = np.dot(xit, self.b[dgt])
        return yy

class LogisticModel(Model):

    def __init__(self, dgt):
        self.dgt = dgt

    def Train(self, xi, y):
        self.b = np.random.random(xi.shape[1]) / 100000
        y2 = [1 if y[i] == self.dgt else 0 for i in range(y.size)]
        y2 = np.array(y2)
        y2t = y2.T
        alpha = 256*256 #xi.shape[1] * y.size * y.size / 2000
        i = 0
        J0 = 0
        J = J0
        trans = xi.T
        for s in range(1000):
            yy = np.dot(xi, self.b)
            g = 1 / (1 + np.exp(-yy))
            diff = g - y2
            if (i % 10 == 0):
                J = -1 * np.dot(y2t, np.log(g)) - np.dot((1 - y2t), np.log(1 - g))
                print("step %s, J = %s, delta = %s %%" % (i, J, (J0 - J) * 100 / (J0 + 0.5)))
            dot = np.dot(trans, diff)
            grad = dot * (2 / y.size)
            self.b -= grad / alpha
            i += 1
            J0 = J
        return self.b

    def Predict(self, xit, dgt):
        if(dgt != self.dgt):
            return None
        yy = np.dot(xit, self.b)
        g = 1 / (1 + np.exp(-yy))
        return g

class SvmModel(Model):

    def __init__(self, dgt):
        self.dgt = dgt

    def Train(self, xi, y):
        x = xi[:, 1:]
        y2 = [1 if y[i] == self.dgt else 0 for i in range(y.size)]
        self.clf = svm.SVC(kernel='linear')
        self.clf.fit(x, np.array(y2))
        print("SVM model trained.")
        return self.clf

    def Predict(self, xit, dgt):
        if(dgt != self.dgt):
            return None
        x = xit[:, 1:]
        yy = self.clf.predict(x)
        return yy

df = pd.read_csv("mnist_train.csv", header=None)
print("Train dataset loaded.")

n = 6000
n = n or df.shape[0]
y = df.values[:n, 0:1]
xi = df.copy().values[:n, :]
xi[:, 0] = 1
print("Train dataset prepared, size = ", y.size)

# for i in np.random.random(20)*n:
#     visualize(xi, y, int(round(i)))

#mod = LinearModel()
mod = LogisticModel(5)
# mod = SvmModel(5)
mod.Train(xi, y)

df = pd.read_csv("mnist_test.csv", header=None)
print("Test dataset loaded.")

n = 1000
n = n or df.shape[0]
xit = df.copy().values[:n, :]
xit[:, 0] = 1
yt = df.copy().values[:n, 0:1]
print("Test dataset prepared, size = ", yt.size)

posmargin = 0.0
negmargin = 0.0
for dgt in range(10):
    y2 = [1 if yt[i] == dgt else 0 for i in range(yt.size)]

    # yy = np.dot(xit, b[dgt])
    yy = mod.Predict(xit, dgt)
    if(yy is None):
        continue

    len = yy.size
    yy = [1 if yy[i] > 0.5 + posmargin else -1 if yy[i] > 0.5 - negmargin else 0 for i in range(len)]

    print()
    print(dgt)

    tp = sum([1.0 if yy[i] == y2[i] and yy[i] == 1 else 0.0 for i in range(len)]) #true positive
    tn = sum([1.0 if yy[i] == y2[i] and yy[i] == 0 else 0.0 for i in range(len)]) #true negative
    fp = sum([1.0 if yy[i] != y2[i] and yy[i] == 1 else 0.0 for i in range(len)]) #false positive
    fn = sum([1.0 if yy[i] != y2[i] and yy[i] == 0 else 0.0 for i in range(len)]) #false negative
    tot = tp + tn + fp + fn #all predicted
    acc = (tp + tn)/tot #how many correct answers, positive or negative?
    precision = tp / (tp + fp) #of all predicted as positive how many correct?
    recall = tp / (tp + fn) #of all positive how many predicted?

    print("Accuracy = ", acc)
    print("TP = %s, TN = %s, FP = %s, FN = %s, TOT = %s, LEN = %s" %(tp, tn, fp, fn, tot, len))
    print("Precision = ", precision)
    print("Recall = ", recall)