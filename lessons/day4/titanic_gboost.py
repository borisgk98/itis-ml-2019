import numpy as np
import pandas as pd
import lightgbm
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import seaborn as sns
import matplotlib.pyplot as plt   #Data visualisation libraries
from sklearn.metrics import confusion_matrix,accuracy_score, roc_curve, auc

train = pd.read_csv('titanic.csv')
train.drop(["PassengerId", "Name", "Ticket"],axis=1,inplace=True)
train.fillna(train.mean(), inplace=True)

train['Cabin'] = train['Cabin'].apply(lambda x: '' if pd.isnull(x) else x)
train['Embarked'] = train['Embarked'].apply(lambda x: '' if pd.isnull(x) else x)

print(train.columns[train.isna().any()].tolist())
cats = train.select_dtypes(include='object').columns
le = preprocessing.LabelEncoder()
for col in cats:
    le.fit(train[col])
    train[col] = le.transform(train[col])

y = train['Survived']
train.drop(["Survived"],axis=1,inplace=True)
x = train.values


#
# Create training and validation sets
#
x, x_test, y, y_test = train_test_split(x, y, test_size=0.3, random_state=42)

#
# Create the LightGBM data containers
#

categorical_features = [c for c, col in enumerate(train.columns) if col in cats]
train_data = lightgbm.Dataset(x, label=y, categorical_feature=categorical_features)
test_data = lightgbm.Dataset(x_test, label=y_test, categorical_feature=categorical_features)


#
# Train the model
#

parameters = {
    'objective': 'binary',
    # 'metric': 'mape', #'root_mean_squared_error',
    'metric': 'binary_error',
    # 'is_unbalance': 'true',
    # 'boosting': 'goss',
    # 'num_leaves': 31,
    'feature_fraction': 0.6,
    'bagging_fraction': 0.5,
    'bagging_freq': 10,
    'learning_rate': 0.05,
    'verbose': 1
}

model = lightgbm.train(parameters,
                       train_data,
                       valid_sets=test_data,
                       num_boost_round=5000,
                       early_stopping_rounds=50)

predictions_lgbm_prob = model.predict(x_test)
predictions_lgbm_01 = np.where(predictions_lgbm_prob > 0.5, 1, 0)

acc_lgbm = accuracy_score(y_test, predictions_lgbm_01)
print('Overall accuracy:', acc_lgbm)

cm = confusion_matrix(y_test, predictions_lgbm_01)
labels = ['No', 'Yes']
plt.figure(figsize=(8,6))
sns.heatmap(cm, xticklabels = labels, yticklabels = labels, annot = True, fmt='d', cmap="Blues", vmin = 0.2);
plt.title('Confusion Matrix')
plt.ylabel('True Class')
plt.xlabel('Predicted Class')
plt.show()
